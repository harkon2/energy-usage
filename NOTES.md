Candidate Name: Harris Kontogiannis

Tasks: Improve the look and feel of the application, Change how monthly usage is calculated.

Time: Almost 3 1/2 hours. 

Notes:
I choose to use the Linear Interpolation algorithm to estimate the usage of the date at the end of month. 
Its not the best approach and there are some wrong estimations shown in the Graph but didn't have time 
to implement something more advanced like the Holt aglorithms.

I would love to implement also the API task (#2) but the server is in KOA and i have never worked with it before.
I spent some time to learn the API but it was taking longer than expected and i decvided to implement the task #3

