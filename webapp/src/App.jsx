import React, { Component } from 'react';
import { string, object } from 'prop-types'
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';
import moment from 'moment'
import meterReadingsData from './data/meterReadingsSample.json';
import './App.scss'
import { transform } from './utils'

const API = 'https://storage.googleapis.com/bulb-interview/meterReadingsReal.json';
const formattedDate = date => moment(date).format("YYYY-MM-DD");

export const Article = ({ title, children, style }) => {
  return (
    <article style={style}>
      <h2 style={{color: 'blue'}}>{title}</h2>
      { children }
    </article>
  )
}

const UsageChart = ({ energyUsageData }) => {
  return (
    <ResponsiveContainer height='100%' aspect={1.0/0.25}>
      <BarChart data={energyUsageData}>
        <XAxis dataKey="date" />
        <YAxis dataKey="energyUsage" />
        <CartesianGrid horizontal={false} />
        <Tooltip />
        <Bar dataKey="energyUsage" fill="#03ad54" isAnimationActive={false} />
      </BarChart>
    </ResponsiveContainer>
  )
}

const UsageTable = ({ meterReadingsRows }) => {
  return (
    <table style={{width: '100%'}}>
      <tbody>
        <tr style={{display: 'flex', flexFlow: 'row nowrap', justifyContent: 'space-around', alignItems: 'center', textAlign: 'left', padding: '5px'}}>
          <th style={{flex: '1 20%'}}>Date</th>
          <th style={{flex: '1 20%'}}>Reading</th>
          <th style={{flex: '1 20%'}}>Estimated Date</th>
          <th style={{flex: '1 20%'}}>Estimated Usage</th>
          <th style={{flex: '1 20%'}}>Unit</th>
        </tr>
        {meterReadingsRows}
      </tbody>
    </table>
  )
}

const getEnergyUsageData = meterReadings => {
  let result = []
  for(let i = 0; i < meterReadings.length - 2; i++) {
    const energyUsage = meterReadings[i+1].estimatedUsage - meterReadings[i].estimatedUsage;
    result.push({
      date: meterReadings[i+1].estmationDate,
      energyUsage
    });
  }
  return result;
}

const getMeterReadingsRows = meterReadings => {
  return meterReadings.map(reading => (
    <tr key={reading.readingDate} style={{display: 'flex', flexFlow: 'row nowrap', justifyContent: 'space-around', alignItems: 'center', padding: '5px'}}>
      <td style={{flex: '1 20%'}}>{formattedDate(reading.readingDate)}</td>
      <td style={{flex: '1 20%', color: 'red', fontWeight: '700'}}>{reading.cumulative}</td>
      <td style={{flex: '1 20%'}}>{reading.estmationDate}</td>
      <td style={{flex: '1 20%', color: 'green', fontWeight: '700'}}>{reading.estimatedUsage}</td>
      <td style={{flex: '1 20%'}}>{reading.unit}</td>
    </tr>
  ));
}


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      readings: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch(API)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => {

        return this.setState({ readings: transform(data.electricity), isLoading: false }) 
      })
      .catch(error => { console.log('error catch', error ); return this.setState({ error, isLoading: false }) });
  }

  render(){

    const containerStyle = {display: 'flex', flexFlow: 'column nowrap', justifyContent: 'space-around', padding: '20px'};
    const chartStyle = {display: 'block', border: '1px solid #f3f3f3', margin: '0 20px', padding: '20px'};
    const tableStyle = {display: 'block', border: '1px solid #f3f3f3', margin: '0 20px', padding: '20px'};

    const { readings, isLoading, error } = this.state;

    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <p>Loading ...</p>;
    }

    return (
      <div style={containerStyle}>
        <Article title = 'Energy Usage' style={chartStyle}>
          <UsageChart energyUsageData={getEnergyUsageData(readings)}/>
        </Article>
        <Article title = 'Meter Readings' style={tableStyle}>
          <UsageTable meterReadingsRows={getMeterReadingsRows(readings)}/>
        </Article>
      </div>
    );
  }
  
};

export default App;
