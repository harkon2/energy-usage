
import moment from 'moment'

export const transform = (readings) => {
	let results = []

  readings.map((reading, i, ts) => {
    
    if (!ts[i-1]) return 

    let month = Object.assign(reading, { readingDate: moment(reading.readingDate) })
    let prevMonth = Object.assign(ts[i - 1], { readingDate: moment(ts[i - 1].readingDate) })
    let target = getDaysUntilMonthEnd(month.readingDate)

    if (!isEndOfMonth(month.readingDate) ){
      let daysDiff = getDiffInDays(month.readingDate, prevMonth.readingDate)
      let usageDiff = month.cumulative - prevMonth.cumulative
      let estimatedUsage = forecast(month.cumulative, target, usageDiff, daysDiff)
      let estmationDate = getDate(month.readingDate, target)
      results.push(Object.assign(month, { estmationDate: estmationDate, estimatedUsage: estimatedUsage }))
    } else {
    	results.push(Object.assign(month, { estmationDate: getDate(month.readingDate, target), estimatedUsage: month.cumulative }))
    }
  })
  return results
}

/*
  Linear Interpolation Algorithm
  y = y1 + (x - x1) * y2 - y1 /x2 - x1
*/
const forecast = (a, b, c, d) => {
  return Math.round(a + b * c / d)
}

function getDate(mmt, days){
	return moment
    	.utc(mmt)
    	.add(days, 'days')
    	.format("YYYY-MM-DD")
}

 	
/**
 * Check whether a moment object is the end of the month.
 * Ignore the time part.
 * @param {moment} mmt
 */
function isEndOfMonth(mmt) {
  // startOf allows to ignore the time component
  // we call moment(mmt) because startOf and endOf mutate the momentj object.
  return moment
    .utc(mmt)
    .startOf('day')
    .isSame(
      moment
        .utc(mmt)
        .endOf('month')
        .startOf('day')
    );
}

/**
 * Returns the difference between two moment objects in number of days.
 * @param {moment} mmt1
 * @param {moment} mm2
 */
function getDiffInDays(mmt1, mm2) {
  return mmt1.diff(mm2, 'days');
}

/**
 * Return the number of days between the given moment object
 * and the end of the month of this moment object.
 * @param {moment} mmt
 */
function getDaysUntilMonthEnd(mmt) {
  return getDiffInDays(moment.utc(mmt).endOf('month'), mmt);
}
