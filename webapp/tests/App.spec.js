import React from 'react';
import { shallow } from 'enzyme';
import App, { Article } from '../src/App';

describe('App', () => {

	const data = {
		"electricity": [
      {"cumulative": 17600, "readingDate": "2017-03-31T00:00:00.000Z", "unit": "kWh" },
      {"cumulative": 17859, "readingDate": "2017-04-30T00:00:00.000Z", "unit": "kWh" }
	  ]
	}

	beforeEach(() => {
	  global.fetch = jest.fn().mockImplementation(() => {
	      var promise = new Promise((resolve, reject) => {
	        resolve({ ok: true, json: () => data })
	      });
	      return promise;
	  });
	});

	it('should load correctly', done => {
    const wrapper = shallow(<App />);
    wrapper.instance().componentDidMount()
    expect(wrapper.text()).toEqual('Loading ...');
    expect(wrapper.state()).toHaveProperty('isLoading', true);

    fetch()
	    .then()
	    .then(()=>{
	    	expect(wrapper.state()).toHaveProperty('isLoading', false);
	    	wrapper.update();
	    	expect(wrapper.find(Article).length).toEqual(2); 
	    	done();
	    })
  });
});

